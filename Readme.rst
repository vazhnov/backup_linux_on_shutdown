Perform backup on Linux system shutdown
#######################################

.. contents::


Description
===========

This is a simple shell-script and init-script that can help you to perform backup of whole Linux workstation on every shutdown.


Compatibility
=============

Works fine with:

- Xubuntu 16.04 (systemd v229)
- Xubuntu 18.04 (systemd v237)
- Gentoo 2020 (OpenRC 0.42)


Setup
=====

Prepare rsyncd server
---------------------

- Add DNS name ``rsync-backup-server``.
- Setup rsync on your server.
- Configure rsync on server, for example:

 - Create system user for backups:
   ::

    sudo mkdir -p /backup_desktop # Or something like 'sudo ln -s /path/to/big/drive /backup_desktop'
    sudo adduser --system --group --home /backup_desktop --no-create-home --disabled-password --gecos "" rsync_backup_desktop
    sudo chown -R rsync_backup_desktop:root /backup_desktop
    sudo chmod 750 /backup_desktop

 - ``/etc/rsyncd.conf``:
   ::

    [backup_comp1]
    comment = Current backup
    path = /backup_desktop/comp1
    use chroot = yes
    max connections=2
    read only = no
    list = yes
    uid = rsync_backup_desktop
    gid = rsync_backup_desktop
    auth users = comp1
    secrets file = /etc/rsyncd.secrets
    strict modes = yes
    hosts allow = 192.168.1.100
    hosts deny =

 - Create ``/etc/rsyncd.secrets`` and set secure permissions:
   ::

    sudo touch /etc/rsyncd.secrets
    sudo chown root:root /etc/rsyncd.secrets
    sudo chmod 600 /etc/rsyncd.secrets


 - Example of ``/etc/rsyncd.secrets`` content:
   ::

    comp1:pass1
    comp2:pass2


Configure client
----------------

OpenRC
~~~~~~

- Install this script on desktop:
  ::

   sudo emerge -n net-misc/rsync
   sudo ./rsync_backup_desktop_install_openrc.sh
   sudo vim /etc/scripts/rsync_backup_desktop.password


OpenRC client customization
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Edit the file ``/etc/conf.d/rsync_backup_desktop``.


Systemd
~~~~~~~

- Install this script on desktop:
  ::

   sudo apt-get install rsync
   sudo ./rsync_backup_desktop_install_systemd.sh
   sudo editor /etc/scripts/rsync_backup_desktop.password
   sudo service rsync_backup_desktop start


Systemd client customization
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can redefine some variables in ``/etc/default/rsync_backup_desktop``, for example:
 ::

  SYNC_PATHS="/etc /var/backups /var/spool /var/lib /home"
  SYNC_SERVER_NAME="rsync-backup-server.example.com"
  RSYNC_OPTIONS="--password-file=/etc/scripts/rsync_backup_desktop.password -a --one-file-system --no-o --no-g --delete --relative"

Or you can set your own script, timeout or ConditionPathExists by creating file ``/etc/systemd/system/rsync_backup_desktop.service.d/mysettings.conf``, for example:
 ::

  [Unit]
  ConditionPathExists=/etc/scripts/rsync_backup_desktop.password

  [Service]
  EnvironmentFile=/etc/default/rsync_backup_desktop
  ExecStop=/usr/bin/rsync_backup_on_shutdown
  TimeoutStopSec=20min


Check
=====

openrc
------

Run script manually:
 ::

  source /etc/conf.d/rsync_backup_desktop
  sudo bash /usr/local/sbin/rsync_backup_on_shutdown

systemd
-------

Start service manually:
 ::

  sudo service rsync_backup_desktop start

Check status:
 ::

  systemctl status rsync_backup_desktop.service

Output should be like:
 ::

  ● rsync_backup_desktop.service - Backup desktop via rsync
     Loaded: loaded (/etc/systemd/system/rsync_backup_desktop.service; enabled; vendor preset: enabled)
     Active: active (exited) since Вс 2017-01-08 11:16:37 +05; 10h ago
       Docs: https://gitlab.com/vazhnov/backup_linux_on_shutdown
    Process: 724 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
   Main PID: 724 (code=exited, status=0/SUCCESS)
     CGroup: /system.slice/rsync_backup_desktop.service

Then stop service, to start copy process:
 ::

  sudo service rsync_backup_desktop stop

Then check status:
 ::

  systemctl status rsync_backup_desktop.service

Output should be like:
 ::

  ● rsync_backup_desktop.service - Backup desktop via rsync
     Loaded: loaded (/etc/systemd/system/rsync_backup_desktop.service; enabled; vendor preset: enabled)
     Active: inactive (dead) since Tue 2018-05-22 20:28:15 +05; 37min ago
       Docs: https://gitlab.com/vazhnov/backup_linux_on_shutdown
    Process: 3103 ExecStop=/usr/bin/rsync_backup_on_shutdown (code=exited, status=0/SUCCESS)
    Process: 784 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
   Main PID: 784 (code=exited, status=0/SUCCESS)


Security
========

TODO: Rsync over SSH.


TODO
====

- Add compatibility with `vbackup <https://github.com/sharhalakis/vbackup>`_


License
=======

`CC0 1.0 <https://creativecommons.org/publicdomain/zero/1.0/>`_ or newer


Known issues
============

- somebody should check recipes for SysV and upstart
