#!/sbin/openrc-run

description="Backup desktop files via rsync."
name="rsync-backup-desktop"
# command="/usr/local/sbin/rsync_backup_on_shutdown"
required_files="/etc/scripts/rsync_backup_desktop.password"

depend()
{
  use localmount netmount
  keyword -containers -shutdown -timeout
}

start()
{
  if [ "$RC_RUNLEVEL" = "shutdown" ] && [ "$RC_REBOOT" != "YES" ]; then
    ebegin "Starting to backup"
    bash /usr/local/sbin/rsync_backup_on_shutdown
  fi
  eend 0
}
