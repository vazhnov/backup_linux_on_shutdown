#!/usr/bin/env bash
set -o nounset
set -o errexit
# set -o pipefail
# shopt -s dotglob

# Author: Alexey Vazhnov <vazhnov@boot-keys.org>
#
# Fresh sources: https://gitlab.com/vazhnov/backup_linux_on_shutdown
#
# License: CC0 1.0 or newer
# https://creativecommons.org/publicdomain/zero/1.0/

sudo cp -p systemd/rsync_backup_desktop.service /etc/systemd/system/rsync_backup_desktop.service
sudo systemctl daemon-reload
sudo systemctl reenable rsync_backup_desktop.service
sudo cp -p rsync_backup_on_shutdown.sh /usr/bin/rsync_backup_on_shutdown
sudo mkdir -p /etc/scripts
sudo touch /etc/scripts/rsync_backup_desktop.password
sudo chmod 640 /etc/scripts/rsync_backup_desktop.password
sudo touch /etc/default/rsync_backup_desktop
echo "Don't forget to add password to /etc/scripts/rsync_backup_desktop.password!"
echo "Don't forget to add password to /etc/rsyncd.secrets on the server!"
echo "Don't forget to create directory /backup_desktop/$(hostname) on the server!"
