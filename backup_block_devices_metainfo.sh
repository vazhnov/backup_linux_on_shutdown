#!/usr/bin/env bash
set -o nounset
# set -o errexit
set -o pipefail
shopt -s dotglob

# Author: Alexey Vazhnov <vazhnov@boot-keys.org>
#
# Fresh sources: https://gitlab.com/vazhnov/backup_linux_on_shutdown
#
# License: CC0 1.0 or newer
# https://creativecommons.org/publicdomain/zero/1.0/

mkdir -p "/var/backups"

if [ -x /bin/lsblk ] && [ -x /sbin/sfdisk ]; then
	for sdX in $(lsblk --nodeps --noheadings --output NAME | xargs); do
		/sbin/sfdisk --quiet --dump "/dev/$sdX" --backup --backup-file /var/backups/sfdisk_backup >/var/backups/sfdisk_dump.txt
		#/sbin/sfdisk --quiet --no-reread --backup /dev/$sdX --backup-file /var/backups/sfdisk_backup
	done
fi

# sgdisk

if [ -x /sbin/mdadm ]; then
	/sbin/mdadm --examine --scan > /var/backups/raid_mdadm_examine_scan.txt
fi

if [ -x /sbin/vgcfgbackup ]; then
	/sbin/vgcfgbackup --file /var/backups/lvm_vgcfgbackup.txt
	/sbin/vgdisplay --partial --verbose > /var/backups/lvm_vgdisplay.txt
	/sbin/lvdisplay --partial > /var/backups/lvm_lvdisplay.txt
fi

if [ -x /usr/sbin/slapcat ]; then
	/usr/sbin/slapcat -l /var/backups/OpenLDAP/LDAP-transfer-LDIF.txt
fi
