#!/usr/bin/env bash
set -o nounset
set -o errexit

# Author: Alexey Vazhnov <vazhnov@boot-keys.org>
#
# Fresh sources: https://gitlab.com/vazhnov/backup_linux_on_shutdown
#
# License: CC0 1.0 or newer
# https://creativecommons.org/publicdomain/zero/1.0/

test -f /sbin/openrc-run || { echo '/sbin/openrc-run not found! Do you really want to install openrc version?'; exit 1; }

# sudo cp -p openrc/rsync_backup_desktop.sh /etc/local.d/rsync_backup_desktop.stop
# install --verbose --preserve-timestamps --owner=root --group=root --mode=0755 --no-target-directory openrc/rsync_backup_desktop.sh /etc/init.d/rsync_backup_desktop
# mkdir -p /etc/conf.d
# install --verbose --preserve-timestamps --owner=root --group=root --mode=0644 --no-target-directory openrc/rsync_backup_desktop.conf.d /etc/conf.d/rsync_backup_desktop
mkdir -p /usr/local/sbin
install --verbose --preserve-timestamps --owner=root --group=root --mode=0755 --no-target-directory rsync_backup_on_shutdown.sh /usr/local/sbin/rsync_backup_on_shutdown
install --verbose --preserve-timestamps --owner=root --group=root --mode=0755 --no-target-directory "openrc/rsync_backup_on_shutdown.stop" "/etc/local.d/rsync_backup_on_shutdown.stop"
mkdir -p /etc/scripts
touch /etc/scripts/rsync_backup_desktop.password
chmod 640 /etc/scripts/rsync_backup_desktop.password
# rc-update delete rsync_backup_desktop shutdown
# rc-update add rsync_backup_desktop shutdown
echo "Don't forget to add password to /etc/scripts/rsync_backup_desktop.password!"
echo "Don't forget to add password to /etc/rsyncd.secrets on the server!"
echo "Don't forget to create directory /backup_desktop/$(hostname) on the server!"
