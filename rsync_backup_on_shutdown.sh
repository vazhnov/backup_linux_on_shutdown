#!/usr/bin/env bash
set -o nounset
# set -o errexit
# set -o pipefail
shopt -s dotglob

# Author: Alexey Vazhnov <vazhnov@boot-keys.org>
#
# Fresh sources: https://gitlab.com/vazhnov/backup_linux_on_shutdown
#
# License: CC0 1.0 or newer
# https://creativecommons.org/publicdomain/zero/1.0/

# exec >>/var/log/rsync_backup_desktop.log 2>&1
# date +%F_%H-%M-%S

USERNAME="$(hostname)"
HOSTNAME="backup_$(hostname)"
# Substitution of default values, if no one set in /etc/default/rsync_backup_desktop (see 'man bash', chapter 'Parameter Expansion'):
SYNC_PATHS=${SYNC_PATHS:-/etc /var/backups /var/spool /var/lib /home}
SYNC_SERVER_NAME=${SYNC_SERVER_NAME:-rsync-backup-server}
# The order of the options is important!
RSYNC_OPTIONS=${RSYNC_OPTIONS:---password-file=/etc/scripts/rsync_backup_desktop.password -a --one-file-system --no-o --no-g --delete --relative}

DESTINATION="$USERNAME@$SYNC_SERVER_NAME::$HOSTNAME"
for DIR in $SYNC_PATHS; do
	test -d "$DIR" || { echo "Warning: no such directory: $DIR, skipped"; continue; }
	test "$(ls -lAF "$DIR" | wc -l)" -gt 2 || { echo "Warning: too little number of files in $DIR, skipped"; continue; }
	echo "##### Starting rsync of ${DIR}"
	# shellcheck disable=SC2086
	rsync ${RSYNC_OPTIONS} "${DIR}/" "${DESTINATION}"
	echo "##### Finished rsync of ${DIR}"
done
sleep 3
